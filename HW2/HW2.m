addpath('C:/Users/Hyukjun Kwon/Desktop/econ_512_2017/CEtools/')
% dont use absolute path, learn to use relative one like ../econ_512
%% question 1
% break if down into sections like I did here

V = [-1,-1,-1]';
P_1 = [1,1,1]';
q_A_1 = exp(V(1)-P_1(1))/(1+exp(V(1)-P_1(1))+exp(V(2)-P_1(2))+exp(V(3)-P_1(3)));
q_B_1 = exp(V(2)-P_1(2))/(1+exp(V(1)-P_1(1))+exp(V(2)-P_1(2))+exp(V(3)-P_1(3)));
q_C_1 = exp(V(3)-P_1(3))/(1+exp(V(1)-P_1(1))+exp(V(2)-P_1(2))+exp(V(3)-P_1(3)));
q_zero_1 = 1/(1+exp(V(1)-P_1(1))+exp(V(2)-P_1(2))+exp(V(3)-P_1(3)));

%% question 2

P_a = [1,1,1]';
P_b = [0,0,0]';
P_c = [0,1,2]';
P_d = [3,2,1]';

tic
[x_aa,fval_a,flag_a,it_a] = broyden(@(P) foc(P),P_a);
[x_b,fval_b,flag_b,it_b] = broyden(@(P) foc(P),P_b);
[x_c,fval_c,flag_c,it_c] = broyden(@(P) foc(P),P_c);
[x_d,fval_d,flag_d,it_d] = broyden(@(P) foc(P),P_d);
toc
% you have a problem with foc function
%% question 4

tic

x_aa = P_a;
for i = 1:10;
    x_aa = [1 / (1 - exp(-1-x_aa(1))/(1+exp(-1-x_aa(1))+exp(-1-x_aa(2))+exp(-1-x_aa(3)))); ...
        1 / (1 - exp(-1-x_aa(2))/(1+exp(-1-x_aa(1))+exp(-1-x_aa(2))+exp(-1-x_aa(3))));...
        1 / (1 - exp(-1-x_aa(3))/(1+exp(-1-x_aa(1))+exp(-1-x_aa(2))+exp(-1-x_aa(3))))];
end


x_bb = P_b;
for i = 1:10;
    x_bb = [1 / (1 - exp(-1-x_bb(1))/(1+exp(-1-x_bb(1))+exp(-1-x_bb(2))+exp(-1-x_bb(3)))); ...
        1 / (1 - exp(-1-x_bb(2))/(1+exp(-1-x_bb(1))+exp(-1-x_bb(2))+exp(-1-x_bb(3))));...
        1 / (1 - exp(-1-x_bb(3))/(1+exp(-1-x_bb(1))+exp(-1-x_bb(2))+exp(-1-x_bb(3))))];
end

x_cc = P_c;
for i = 1:10;
    x_cc = [1 / (1 - exp(-1-x_cc(1))/(1+exp(-1-x_cc(1))+exp(-1-x_cc(2))+exp(-1-x_cc(3)))); ...
        1 / (1 - exp(-1-x_cc(2))/(1+exp(-1-x_cc(1))+exp(-1-x_cc(2))+exp(-1-x_cc(3))));...
        1 / (1 - exp(-1-x_cc(3))/(1+exp(-1-x_cc(1))+exp(-1-x_cc(2))+exp(-1-x_cc(3))))];
end

x_dd = P_d;

for i = 1:10;
    x_dd = [1 / (1 - exp(-1-x_dd(1))/(1+exp(-1-x_dd(1))+exp(-1-x_dd(2))+exp(-1-x_dd(3)))); ...
        1 / (1 - exp(-1-x_dd(2))/(1+exp(-1-x_dd(1))+exp(-1-x_dd(2))+exp(-1-x_dd(3))));...
        1 / (1 - exp(-1-x_dd(3))/(1+exp(-1-x_dd(1))+exp(-1-x_dd(2))+exp(-1-x_dd(3))))];
end

toc

% you should give some more info on how many iterations it took etc.
