function fval = foc(P)
q_a = exp(-1-P(1))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));
q_b = exp(-1-P(2))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));
q_c = exp(-1-P(3))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));
foc_a = -q_a*(1-q_a);
foc_b = -q_b*(1-q_b);
foc_c = -q_c*(1-q_c);
fval = [foc_a; foc_b; foc_c];    
end