addpath('C:/Users/Hyukjun Kwon/Desktop/econ_512_2017/CEtools/')
% Please use the relative path, not absolute one


% Part A

Y=1.1:0.01:3;
theta1=zeros(length(Y),1);
t=ones(1000,1);
for i=1:length(Y);
    for j=1:1000;
        t(j+1) = t(j)-(log(t(j))-log(Y(i))-psi(t(j)))/(1/t(j) - psi(1,t(j)));
        theta1(i)=t(1000);
    end
end
plot(Y,theta1);
% This does not estimate theta


% Part B

load('HW3.mat');

% 1

likeli = @(beta)-sum(-exp(X*beta)+y.*(X*beta)-log(factorial(y)));

option1 = optimoptions('fminunc','Display','iter');
beta1 = fminunc(likeli,[1;1;1;1;1;1],option1);


option2 = optimoptions('fminunc','Display','iter','SpecifyObjectiveGradient',true);
beta2 = fminunc(@(beta) withgrad(beta,X,y),[1;1;1;1;1;1],option2);

option3 = optimset('Display','iter','PlotFcns',@optimplotfval);
[beta3,fval,exitflag,output] = fminsearch(likeli,[1;1;1;1;1;1],option3);

% 3
nlls = @(beta) -sum( (y-exp(X*beta)).^(2));
beta4 = fminsearch(nlls, [1;1;1;1;1;1]);

% None of these procedures arrived any close to the true solution