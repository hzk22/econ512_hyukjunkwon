function [f,g] = withgrad(beta,X,y)
    f = -sum(-exp(X*beta)+y.*(X*beta)-log(factorial(y)));
    if nargout > 1
        h=zeros(length(X),6);
        for i=1:length(X);
            h(i,:)= -X(i,:)*exp(X(i,:)*beta)+y(i)*X(i,:);
        end
        g=sum(h);
end