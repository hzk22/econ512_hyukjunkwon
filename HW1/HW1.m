kdiary

% problem 1

X = [1 1.5 3 4 5 7 9 10];
Y1 = -2 + .5*X;
X2 = X.^2;
Y2 = -2 + .5*X2;
plot(X,Y1)
hold on
plot(X,Y2)
hold off

% problem 2

Z = linspace(-10, 20, 200);
SumZ = sum(Z);

% problem 3

A = [2 4 6; 1 7 5; 3 12 4];
B = [-2 3 10]';
C = A'*B;
% inv is very slow. Use \ like in the answer key D = (A'*A)\B
D = inv(A'*A)*B;
E = sum(A(1,:)*B(1)+A(2,:)*B(2)+A(3,:)*B(3));
F = [A(1,1:2);A(3,1:2)];
x = A\B;

% problem 4

G = kron(eye(5), A);

% problem 5

H = random('Normal',10,5,5,3);
% faster way to do is H = H>10
for i=1:5
    for j=1:3
        if H(i,j)<10
            H(i,j)=0;
        else
            H(i,j)=1;
        end
    end
end

% problem 6

load datahw1.csv;
YY = datahw1(:,5);
XX1 = datahw1(:,3);
XX2 = datahw1(:,4);
XX3 = datahw1(:,6);
XX = [XX1 XX2 XX3];
fitlm(XX,YY)

diary