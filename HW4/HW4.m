load('hw4data.mat')
load('X.mat')
load('Y.mat')
load('Z.mat')
addpath('C:\Users\Hyukjun Kwon\Desktop\Econ_512_HyukjunKwon\econ_512_2017\CompEcon\CEtools')

%% 1. 

b = 0.1 + randn(1,100);
f = @(b) likeli(b).*normpdf(b-0.1);
[x1,w1] = qnwlege(20,-1000,1000);

%% 2.
[x2,w2] = qnwequi(100,-1000,1000,'R');

%% 4. 

g = @(b) likeli2(b).*normpdf(b-0.1);
[x4,w4] = qnwequi(100,-1000,1000,'R');