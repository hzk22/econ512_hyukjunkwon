function L = likeli(b)
load('X.mat');
load('Y.mat');
load('Z.mat');
LL=zeros(20,100);
for t=1:20;
LL(t,:)=((1./(1+exp(-b.*X(t,:)))).^(Y(t,:))).*((1-(1./(1+exp(-b.*X(t,:))))).^(1-Y(t,:)));
end
L=prod(LL);
end