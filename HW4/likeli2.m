function L = likeli2(b)
load('X.mat');
load('Y.mat');
load('Z.mat');
mu = [0.1 0];
sigma = [1 0.5; 0.5 1];
R = chol(sigma);
para = repmat(mu,100,1) + randn(100,2)*R;
b4=para(:,1);
u4=para(:,2);

LL=zeros(20,100);
for t=1:20;
LL(t,:)=((1./(1+exp(-b.*X(t,:)-u4))).^(Y(t,:))).*((1-(1./(1+exp(-b.*X(t,:)-u4)))).^(1-Y(t,:)));
end
L=prod(LL);
end